var mainState = {
    preload: function() { 
	    game.load.image('bird', 'assets/rocket.png'); 
		game.load.image('pipe', 'assets/green-pipe.png');
		game.load.audio('dead', 'assets/jump.wav');
		game.load.audio('music', 'assets/music.wav');
    },
	
	create: function() {
		game.stage.backgroundColor = '#67aadf';
		game.physics.startSystem(Phaser.Physics.ARCADE);
		this.bird = game.add.sprite(100, 245, 'bird');
		game.physics.arcade.enable(this.bird);
		var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
		upKey.onDown.add(this.up, this);
		var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
		downKey.onDown.add(this.down, this);
		upKey.onUp.add(this.stop, this);
		downKey.onUp.add(this.stop, this);
		this.pipes = game.add.group();
		this.timer = game.time.events.loop(1500, this.addRowOfPipes, this); 
		this.score = 0;
		this.labelScore = game.add.text(20, 20, "0", { font: "30px Arial", fill: "#ffffff" });
		this.deadSound = game.add.audio('dead'); 
		this.music = game.add.audio('music');
		this.music.play();
	},
	
	update: function() {
		if (this.bird.y <=0 || this.bird.y >= 490) {
			this.stop();
		}
		
		game.physics.arcade.overlap(this.bird, this.pipes, this.hitPipe, null, this);
	},
	
	up: function() {
		if (this.bird.alive == false)
			return;
		
		this.bird.body.velocity.y = -350;
	},
	
	down: function() {
		if (this.bird.alive == false)
			return;
		
		this.bird.body.velocity.y = 350;
	},
	
	stop: function() {
		this.bird.body.velocity.y = 0;
	},
	
	addOnePipe: function(x, y) {
		var pipe = game.add.sprite(x, y, 'pipe');

		this.pipes.add(pipe);

		game.physics.arcade.enable(pipe);
		
		pipe.body.velocity.x = -300-this.score*10; 
	
		pipe.checkWorldBounds = true;
		pipe.outOfBoundsKill = true;
	},
	
	addRowOfPipes: function() {
		var hole = Math.floor(Math.random() * 5) + 1;

		for (var i = 0; i < 8; i++)
			if (i != hole && i != hole + 1) 
				this.addOnePipe(400, i * 60 + 10);   
		
		this.score += 1;
		this.labelScore.text = this.score; 
	},
	
	hitPipe: function(score) {
		if (this.bird.alive == false)
			return;
		
		this.bird.alive = false;
		
		this.deadSound.play(); 
		
		game.time.events.remove(this.timer);
		
		this.pipes.forEach(function(p){
			p.body.velocity.x = 0;
		}, this);

		this.restartGame(score);
	},
	
	restartGame: function(score) {
		this.music.pause();
		game.state.start('main');
	}
}

var game = new Phaser.Game(400, 490);

var highscore=0;

game.state.add('main', mainState); 

game.state.start('main');